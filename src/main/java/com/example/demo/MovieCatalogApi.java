package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController

@RequestMapping("/api/catalog")

public class MovieCatalogApi {



    @Autowired

    private MovieInformationService movieInformationService;

    @Autowired

    private RatingService ratingService;



    @GetMapping("/{userId}")

    public List<MovieCatalog> getAllMovies(

            @PathVariable String userId) {







        // get all books by userId

        UserMovie  userMovie = movieInformationService.getUserMovies(userId);



        List<MovieCatalog> movieCatalogList = new ArrayList<>();

        for (Movie movie : userMovie.getUserMovies()) {

            Rating movieRating = ratingService.getMovieRating(movie.getId());



            movieCatalogList.add(new MovieCatalog(movie.getTitle(),

                    movie.getAuthor(), movieRating.getRating()));

        }



        return movieCatalogList;

    }





}
