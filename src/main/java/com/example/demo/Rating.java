package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter

@Setter

public class Rating {



    public Rating() {

    }



    public Rating(String movieId, Integer rating) {

        this.movieId = movieId;

        this.rating = rating;

    }



    private String movieId;

    private Integer rating; // 0 to 5

}
