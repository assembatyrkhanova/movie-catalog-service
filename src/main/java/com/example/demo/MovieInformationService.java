package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service

public class MovieInformationService {



    @Autowired

    RestTemplate restTemplate;



    @HystrixCommand(

            fallbackMethod = "getUserBooksFallback",

            threadPoolKey = "getUserBooks",

            threadPoolProperties = {

                    @HystrixProperty(name="coreSize", value="100"),

                    @HystrixProperty(name="maximumSize", value="120"),

                    @HystrixProperty(name="maxQueueSize", value="50"),

                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true"),

            }

    )

    public UserMovie getUserMovies(String userId) {

        return restTemplate.getForObject(

                "http://movie-info-service/movie/info/" + userId,

                UserMovie.class);

    }



    public UserMovie getUserMovieFallback(String userId) {

        UserMovie userMovie = new UserMovie();

        List<Movie> list = new ArrayList<>();

        list.add(new Movie("-1", "Not available", "Not available", "Not available", "Not available"));

        userMovie.setUserMovies(list);



        return userMovie;

    }

}
