package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service

public class RatingService {



    @Autowired

    private RestTemplate restTemplate;



    @HystrixCommand(

            fallbackMethod = "getBookRatingFallback",

            threadPoolKey = "getBookRating",

            threadPoolProperties = {

                    @HystrixProperty(name="coreSize", value="100"),

                    @HystrixProperty(name="maximumSize", value="120"),

                    @HystrixProperty(name="maxQueueSize", value="50"),

                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true"),

            })

    public Rating getMovieRating(String movieId) {

        return restTemplate.getForObject(

                "http://movie-ratings-service/rating/" + movieId,

                Rating.class);

    }

    public Rating getMovieRatingFallback(String movieId) {

        return new Rating(movieId, 0);

    }

}
