package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter

@Setter

public class MovieCatalog {



    public MovieCatalog() {

    }



    public MovieCatalog(String title, String author, Integer rating) {

        this.title = title;

        this.author = author;

        this.rating = rating;

    }



    private String title;

    private String author;

    private Integer rating;

}
