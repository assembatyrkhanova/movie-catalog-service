package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication

@EnableEurekaClient

@EnableCircuitBreaker

@EnableHystrixDashboard
public class DemoApplication {

    @Bean

    @LoadBalanced

    public RestTemplate getRestTemplate() {

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

        requestFactory.setConnectTimeout(3000);

        return new RestTemplate(requestFactory);

    }



    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
