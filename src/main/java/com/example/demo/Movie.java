package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter

@Setter

public class Movie {



    public Movie() {

    }



    public Movie(String id, String title, String genre, String author, String description) {

        this.id = id;

        this.title = title;

        this.genre = genre;
        this.author = author;

        this.description = description;

    }



    private String id;

    private String title;

    private  String genre;

    private String author;

    private String description;

}
